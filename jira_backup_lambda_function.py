# --------- Information ---------
#
# Script created by Piotr Zadrozny
# based on script created by The Epic Battlebeard 10/08/18 and downloaded from https://bitbucket.org/atlassianlabs/automatic-cloud-backup/src/master/jira_backup.py
# thanks to Lukasz Grobelny for correcting the urllib3 connection headers 
#
# this script will trigger and download a backup of a JIRA Cloud site to AWS S3
# script can be run as AWS Lambda function and triggered by scheduled AWS EventBridge rule.
# script uses Amazon SNS to notify admin via email in case of failure
# script uses AWS Secret Manager to store authentication token
# script require proper permission configured in AWS IAM to retrieve data from AWS Secret Manager, send notification via SNS and write to S3 buckets
#
# AWs Lambda will timeout after maximum 15 minutes which can be to short to perform full backup generation and downloading to S3.
# If so I suggest splitting into 2 separate scripts: 1 to generate backup and second to download and save it to S3.
# If the time constraint is still an issue you can run this script in i.e Fargate container or bare EC2 instance without such limitations. 
#

# --------- Roadmap ---------
#
# Proper error handling
# Provide error/exception specific information to notification message
# Create installation and configuration manual for Atlassian API Token, AWS IAM, AWS S3, AWS Lambda, AWS SNS, AWS EventBridge, AWS Secret Manager
#

# --------- Change log ---------
#
# Store Atlassian account token in AWS Secret Manager
# Send notification via AWS SNS
#

import json
import time
import boto3
import logging
import requests
import re
import urllib3
from botocore.exceptions import ClientError

log = logging.getLogger()
log.setLevel(logging.INFO)


def lambda_handler(event, context):
    # 1 Read the input parameters
    account = event['Account']  # Atlassian subdomain i.e. whateverproceeds.atlassian.net
    username = event['Username']  # username with domain something@domain.com
    attachments = event['Attachments']  # Tells the script whether or not to pull down the attachments as well
    cloud = event['Cloud']  # Tells the script whether to export the backup for Cloud or Server
    bucket = event['Bucket']  # Destination S3 bucket to store downloaded backup zipped file
    region = event['Region']  # AWS region i.e. us-west-2
    secret_arn = event['SecretARN']  # AWS Secrets Manager's Secret ARN containing Atlassian Token
    secret_key = event['SecretKey']  # AWS Secrets Manager's Key containing Atlassian Token
    sns_arn = event['TopicARN']  # AWS Simple Notification Service Topic ARN

    # 2 Create convenient variables and open new session for cookie persistence and auth
    atlassian_json_data = json.dumps({"cbAttachments": attachments, "exportToCloud": cloud})  # Atlassian backup endpoint specific parameters
    url = 'https://' + account + '.atlassian.net'  # Create the full base url for the JIRA instance using the account name.

    # 3 Get Atlassian user token from AWS Secret Manager. You can create token using this instruction https://confluence.atlassian.com/cloud/api-tokens-938839638.html
    session = boto3.session.Session()
    client = session.client(service_name='secretsmanager', region_name=region)
    try:
        response = client.get_secret_value(SecretId=secret_arn)
    except ClientError as e:
        raise e
    else:
        secret = response['SecretString']
        secret_json = json.loads(secret)
        token = secret_json[secret_key]

    # 4 Open new session for cookie persistence and auth
    session = requests.Session()
    session.auth = (username, token)
    session.headers.update({"Accept": "application/json", "Content-Type": "application/json"})

    try:
        # 5 Start generating backup
        backup_req = session.post(url + '/rest/backup/1/export/runbackup', data=atlassian_json_data)

        # 6 Catch error response from backup start and exit if error found
        if 'error' in backup_req.text:
            log.info(backup_req.text)
            exit(1)

        # 7 Get task ID of backup.
        task_req = session.get(url + '/rest/backup/1/export/lastTaskId')
        task_id = task_req.text

        # 8 set starting task progress values outside of while loop and if statements.
        task_progress = 0
        last_progress = -1
        global progress_req

        # 9 Get progress and continue until backup generation complete
        while task_progress < 100:
            progress_req = session.get(url + '/rest/backup/1/export/getProgress?taskId=' + task_id)

            # Chop just progress update from json response
            try:
                task_progress = int(re.search('(?<=progress":)(.*?)(?=,)', progress_req.text).group(1))
            except AttributeError:
                log.info(progress_req.text)
                exit(1)

            if (last_progress != task_progress) and 'error' not in progress_req.text:
                log.info(task_progress)
                last_progress = task_progress
            elif 'error' in progress_req.text:
                log.info(progress_req.text)
                exit(1)

            if task_progress < 100:
                time.sleep(10)

        # 10 Download generated backup zip file and stream it to s3 bucket
        if task_progress == 100:
            download = re.search('(?<=result":")(.*?)(?=\",)', progress_req.text).group(1)
            downloadurl=url + '/plugins/servlet/' + download
            date = time.strftime("%Y%m%d_%H%M%S")
            key = account + '_backup_' + date + '.zip'
            s3=boto3.client('s3')
            http=urllib3.PoolManager()
            headers = urllib3.make_headers(basic_auth=username + ":" + token)
            s3.upload_fileobj(http.request('GET', downloadurl, preload_content=False, headers=headers), bucket, key)

    except Exception as e:
        raise e
        # 11 Creating notification in case of failure
        notification_message = "Failed to create backup of Jira Cloud instance!\nInstance URL: " + url + "\nLambda URL: https://" + region + ".console.aws.amazon.com/lambda"
        sns = boto3.client('sns')
        sns.publish(
           TargetArn=sns_arn,
           Message=json.dumps({'default': notification_message}),
           MessageStructure='json'
        )